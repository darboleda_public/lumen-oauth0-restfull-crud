# OAuth0 RESTFULL Lumen

## Official Documentation

1. composer install
2. php artisan migrate
3. Go to app\Http\Middleware\Auth0Middleware.php and put your valid_audiences and authorized_iss.

## License

Licensed under the [MIT license](http://opensource.org/licenses/MIT)
